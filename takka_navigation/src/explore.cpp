#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <boost/program_options.hpp>
#include <tf/transform_listener.h>

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

double random_uniform (double start, double end)
{
  // 0..1
  double x = (double) rand() / (double) RAND_MAX;
  return start + x * (end - start);
}

void worldToRobotCoordinates(geometry_msgs::PointStamped &pointInBotCoords,
			     double y, double x, std::string target_frameid, std::string source_frameid, tf::TransformListener &tf) {
  geometry_msgs::PointStamped whereShouldRobotGo;
  whereShouldRobotGo.header.frame_id = source_frameid;
  //we'll just use the most recent transform available for our simple example
  whereShouldRobotGo.header.stamp = ros::Time();	    
  //just an arbitrary point in space
  whereShouldRobotGo.point.x = x;
  whereShouldRobotGo.point.y = y;
  whereShouldRobotGo.point.z = 0.0;
  tf.transformPoint(target_frameid, whereShouldRobotGo, pointInBotCoords);	    
}

int main(int argc, char** argv){
  namespace po = boost::program_options;
  int opt;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("radius", po::value<int>(&opt)->default_value(2), 
     "Area radius")
    ("points_per_line", po::value<int>(&opt)->default_value(3), 
     "No of points in row/column to visit");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);    

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  double radius = boost::any_cast<int> (vm["radius"].value());
  int points_per_line = boost::any_cast<int> (vm["points_per_line"].value());
  
  ros::init(argc, argv, "simple_navigation_goals");
  tf::TransformListener tf;
  srand(time(NULL));

  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac("move_base", true);
  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  move_base_msgs::MoveBaseGoal goal;

  std::string source_frameid = std::string("odom");
  std::string target_frameid = std::string("bot0/base_footprint");

  // Wait for up to one second for the first transforms to become avaiable. 
  //  tf.waitForTransform(source_frameid, target_frameid, ros::Time(), ros::Duration(1.0));

  ros::NodeHandle nh;
  double rate_hz = 1.0;
  ros::Rate rate(rate_hz);

  double startX = -radius;
  double width = 2 * radius ;
  double startY = -radius;
  double height = 2 * radius;

  double subarea_width = width / float(points_per_line);
  double subarea_height = height / float(points_per_line);

  for (int i = 0; i < points_per_line && nh.ok(); i++ ) {
    bool is_i_odd = i % 2 == 0;
    
    for (int j = is_i_odd ? 0 : points_per_line - 1;
	 (is_i_odd ? j < points_per_line : j >= 0)
	   && nh.ok();
	 is_i_odd ? j++ : j-- )
      {
	try
	  {
	    tf::StampedTransform echo_transform;
	    tf.lookupTransform(source_frameid, target_frameid, ros::Time(), echo_transform);
	    tf::Vector3 v = echo_transform.getOrigin();
	    

	    double x = random_uniform(startX + subarea_width * i, startX + subarea_width * (i + 1)  );
	    double y = random_uniform(startY + subarea_height * j, startY + subarea_height * (j + 1)  );
	    geometry_msgs::PointStamped pointInBotCoords;
	    worldToRobotCoordinates(pointInBotCoords, y, x, target_frameid, source_frameid, tf);

	    goal.target_pose.header.frame_id = target_frameid;
	    goal.target_pose.header.stamp = ros::Time::now();
	    goal.target_pose.pose.position.x = pointInBotCoords.point.x;
	    goal.target_pose.pose.position.y = pointInBotCoords.point.y;		     
	    goal.target_pose.pose.orientation.w = 1.0;


	    std::cout << "Bot  position:                [" << v.getX() << ", " << v.getY() << "]" << std::endl;
	    std::cout << "Going to (world coordinates): [" << x << ", " << y << "]" << std::endl;
	    std::cout << "Going to ( bot coordianates): ["
		      << goal.target_pose.pose.position.x << ", "
		      << goal.target_pose.pose.position.y << "]" << std::endl;
	    
	    ROS_INFO("Sending goal");
	    ac.sendGoal(goal);
	    ac.waitForResult();
			   
	    if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
	      ROS_INFO("Goal reached");
	    else
	      ROS_INFO("The base failed to reach goal");
	    
	  }
	catch(tf::TransformException& ex)
	  {
	    std::cout << "Failure at "<< ros::Time::now() << std::endl;
	    std::cout << "Exception thrown:" << ex.what()<< std::endl;
	    std::cout << "The current list of frames is:" <<std::endl;
	    std::cout << tf.allFramesAsString()<<std::endl;
	    
	  }
	rate.sleep();
      }
  }
  return 0;
}
