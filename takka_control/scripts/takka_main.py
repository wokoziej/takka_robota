#!/usr/bin/env python

import roslib; ### roslib.load_manifest('smach_tutorials')
import rospy
import smach
import smach_ros

import random

                    
class Manual(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['stop'])
        self.counter = 0

    def execute(self, userdata):
        rospy.loginfo('Executing mode manual')
        rate = rospy.Rate(1) # 1hz
        while not self.modeChangedToAuto():
            rate.sleep()
            rospy.loginfo('Still executing mode manual')            
        return 'stop'
    
    def modeChangedToAuto(self):
        return random.randint(0, 1) == 1
    
class Stop(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['manual', 'auto'])

    def execute(self, userdata):
        rospy.loginfo('Stopping')
        rate = rospy.Rate(1) # 1hz
        rate.sleep()
        return self.currentMode ();
    
    def currentMode(self):
        if random.randint(0, 1) == 1:
            return "auto"
        else:
            return "manual";

class Auto(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['stop'])
        self.counter = 0

    def execute(self, userdata):
        rospy.loginfo('Executing mode auto')
        while not self.modeChangedToManual():
            rate = rospy.Rate(1) # 1hz
            rate.sleep()
            rospy.loginfo('Still executing mode auto')            
        return 'stop'

    def modeChangedToManual(self):
        return random.randint(0, 1) == 0
    

# main
def main():

    # First you create a state machine sm
    # .....
    # Creating of state machine sm finished
    

# Execute the state machine
#outcome = sm.execute()

# Wait for ctrl-c to stop the application
#rospy.spin()
#sis.stop()

    rospy.init_node('takka_main_sm')

    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=[])

    # Open the container
    with sm:
        # Add states to the container
        smach.StateMachine.add('MANUAL', Manual(), 
                               transitions={'stop':'STOP'})

        smach.StateMachine.add('AUTO', Auto(), 
                               transitions={'stop':'STOP'})
        
        smach.StateMachine.add('STOP', Stop(), 
                               transitions={'auto':'AUTO', 'manual':'MANUAL'})


    # Create and start the introspection server
    sis = smach_ros.IntrospectionServer('introspection_server', sm, '/SM_ROOT')
    sis.start()

    # Execute SMACH plan
    outcome = sm.execute()






if __name__ == '__main__':
    main()
