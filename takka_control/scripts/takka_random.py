#!/usr/bin/env python
import rospy, sys
from geometry_msgs.msg import Twist

import random

if __name__=="__main__":
    try:
        random.seed(a=None)    
        rospy.init_node('teleop', anonymous=True)
        pub = rospy.Publisher('/cmd_vel', Twist, queue_size=5)
        rate = rospy.Rate(1) # 1hz
        while not rospy.is_shutdown():
            control_speed = random.randint(-1, 1) * random.random()
            control_turn = random.randint(-1, 1) * random.random()
            twist = Twist()
            twist.linear.x = control_speed;
            twist.linear.y = 0;
            twist.linear.z = 0
            twist.angular.x = 0;
            twist.angular.y = 0;
            twist.angular.z = control_turn
            pub.publish(twist)
            rate.sleep()
    except:
        twist = Twist()
        twist.linear.x = 0; twist.linear.y = 0; twist.linear.z = 0
        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
        pub.publish(twist)        
        print "Unexpected error:", sys.exc_info()[0]
    


