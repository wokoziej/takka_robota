### What is this repository for? ###

Play with akka (?) and ROS to drive swarm of robots

### Play manually

After you started gazebo node you can spawn more models of tekka using launch file

```
#!bash
roslaunch takka_gazebo takka_spawner.launch model_name:=tekka_2 x:=1 y:=1
```


Then you can controll your tekka_bot by keybord control using script
```
#!bash
rosrun takka_control takka_key.py /cmd_vel:=/tekka_2/cmd_vel
```
You can make the robot choose random path
```
#!bash
rosrun takka_control takka_random.py /cmd_vel:=/tekka_2/cmd_vel
```
### Filling empty world by randomly moving robots

Start empty world
```
#!bash
roslaunch takka_gazebo takka_empty_world.launch
```
Run script to populate world by robots and drive them randomly 
You can choose number of robots and size of area where robots will apear.
```
#!bash
rosrun takka_control takka_populate.py --num_of_bots=50 --area_radius=2
```
If you want to visualize first robot in rviz 
```
#!bash
roslaunch takka_description view_mobile_robot.launch ns:=/bot0/
```

### Navigation 
We can start gazebo (willowgarage.world), gmapping, rviz and move\_base using single launch file
```
#!bash
roslaunch takka_navigation takka_navigation.launch 
```

Then you can use rviz to point 2DNavGol and observe how map is updated and robot is moving to desired point

### Exploring world
You can use rosrun and explore to unhide willow garage map
```
#!bash
rosrun takka_navigation explore --help

Allowed options:
  --help                     produce help message
  --radius arg (=2)          Area radius
  --points_per_line arg (=3) No of points in row/column to visit


```
